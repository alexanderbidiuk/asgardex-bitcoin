# v.0.5.0 (2020-10-07)

## Breaking changes:

- Rename / add types for `electrs` API: `VOut`, `VIn`, `UtxoStatus`

## Fix:

- Export types of `electrs-api-types.ts` properly

# v.0.4.0 (2020-10-06)

## Add:

- `getExplorerUrl` by using `blockstream`

## Breaking changes:

- `normalTx` takes `NormalTxParams` as its parameter
- `vaultTx` takes `VaultTxParams` as its parameter

First release

# v.0.1.0 (2020-05-11)

First release
